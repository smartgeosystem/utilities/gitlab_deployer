package event_handler

import (
	"github.com/google/shlex"
	log "github.com/sirupsen/logrus"
	"gitlab.com/smartgeosystem/utilities/gitlab_deployer/conf"
	"gopkg.in/go-playground/webhooks.v5/gitlab"
	"net/http"
	"os/exec"
)

func HandleGitLabEvent(debug bool, gitlabToken string, actions []conf.ProjectActions, notification conf.NotificationConfig) func(http.ResponseWriter, *http.Request) {
	hook, _ := gitlab.New()
	if gitlabToken != "" {
		gitlab.Options.Secret(gitlabToken)(hook)
	}
	return func(w http.ResponseWriter, r *http.Request) {
		payload, err := hook.Parse(r, gitlab.PipelineEvents)
		if err != nil {
			log.Errorln(err)
			return
		}
		pipelinePayload := payload.(gitlab.PipelineEventPayload)
		if debug {
			log.Println("New pipeline event: ", pipelinePayload)
		}
		action := ""
		for _, prj := range actions {
			if prj.ProjectName == pipelinePayload.Project.PathWithNamespace {
				action = prj.Actions[pipelinePayload.ObjectAttributes.Ref]
			}
		}
		if action == "" {
			log.Errorf("Project [%s] or ref [%s] not found in config file", pipelinePayload.Project.PathWithNamespace, pipelinePayload.ObjectAttributes.Ref)
			return
		}
		switch pipelinePayload.ObjectAttributes.Status {
		case "running":
			mess, err := notification.Messages.FillMessage(notification.Messages.RunningMessage, pipelinePayload)
			if err != nil {
				return
			}
			err = notification.Telegram.SendMessageWithRetry(mess, notification.Retry)
			if err != nil {
				return
			}
			return
		case "failed":
			mess, err := notification.Messages.FillMessage(notification.Messages.FailedMessage, pipelinePayload)
			if err != nil {
				return
			}
			err = notification.Telegram.SendMessageWithRetry(mess, notification.Retry)
			if err != nil {
				return
			}
			return
		case "success":
			mess, err := notification.Messages.FillMessage(notification.Messages.SuccessMessage, pipelinePayload)
			if err != nil {
				return
			}
			err = notification.Telegram.SendMessageWithRetry(mess, notification.Retry)
			if err != nil {
				return
			}
			go func() {
				commandArgs, err := shlex.Split(action)
				if err != nil && debug {
					log.Errorln("Error on parse action: ", err)
				}
				output, err := exec.Command(commandArgs[0], commandArgs[1:]...).CombinedOutput()
				if debug {
					log.Info("Output: ", string(output))
				}
				if err == nil {
					mess, err := notification.Messages.FillMessage(notification.Messages.DeploySuccessMessage, pipelinePayload)
					if err != nil {
						return
					}
					err = notification.Telegram.SendMessageWithRetry(mess, notification.Retry)
					if err != nil {
						return
					}
				} else {
					errorText := ""
					_, ok := err.(*exec.ExitError)
					if ok {
						errorText = string(output)
					} else {
						errorText = err.Error()
					}
					log.Errorln(errorText)
					mess, err := notification.Messages.FillMessage(notification.Messages.DeployFailedMessage, pipelinePayload)
					if err != nil {
						return
					}
					err = notification.Telegram.SendMessageWithRetry(mess, notification.Retry)
					if err != nil {
						return
					}
				}
			}()
			return
		}
	}
}
