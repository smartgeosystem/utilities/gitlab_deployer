# Gitlab Deployer
A simple daemon for execute simple shell actions (exec script or command) at gitlab pipeline (gitlab ci) events.

## Usage
1. Fill config
2. Create actions\scripts
3. Activate webhook on gitlab
4. Start daemon
