package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

var gitlabToken string
var externalAddr string

// createHookCmd represents the create webhook command
var createHookCmd = &cobra.Command{
	Use:   "create-hook",
	Short: "Create webhook on the gitlab instance",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		createWebHook()
	},
}

func init() {
	rootCmd.AddCommand(createHookCmd)
	createHookCmd.PersistentFlags().StringVar(&gitlabToken, "token", "", "GitLab API token")
	createHookCmd.PersistentFlags().StringVar(&externalAddr, "external-addr", "", "Deployer external address")
}

func createWebHook() {
	// Global and local configuration
	loadConfigs()
	if gitlabToken == "" {
		log.Fatalln("Empty Gitlab Token")
	}
	if externalAddr == "" {
		log.Fatalln("Empty external deployer address")
	}

	// Gitlab API client
	gitClient, err := gitlab.NewClient(gitlabToken, gitlab.WithBaseURL(cfg.GitLab.Address))
	if err != nil {
		log.Fatalln(err)
	}

	// Check and create web-hooks
	for _, prjAction := range cfg.Actions {
		var project, _, _ = gitClient.Projects.GetProject(prjAction.ProjectName, nil, nil)
		log.Printf("Check webhooks for '%s'...\n", project.Name)

		hooks, _, err := gitClient.Projects.ListProjectHooks(project.ID, nil)
		if err != nil {
			log.Fatal(err)
		}
		hookExists := false
		for _, hook := range hooks {
			if hook.URL == externalAddr {
				log.Printf("Hook already exists for such external addr: %s %s. Skip...",
					hook.CreatedAt.String(), hook.URL)
				hookExists = true
				break
			}
		}
		if hookExists {
			continue
		} else {
			log.Println("Try to create new webhook...")
			// Create new pipelines hook
			enableSSLVerification := false
			pipelineEvents := true
			pushEvents := false
			token := cfg.GitLab.Token
			url := externalAddr
			newHook := gitlab.AddProjectHookOptions{
				EnableSSLVerification: &enableSSLVerification,
				PipelineEvents:        &pipelineEvents,
				PushEvents:            &pushEvents,
				Token:                 &token,
				URL:                   &url,
			}
			createdHook, _, err := gitClient.Projects.AddProjectHook(project.ID, &newHook, nil)
			if err != nil {
				log.Fatalln("Error on create new hook", err)
			}
			log.Printf("Webhook created for: '%s' %s", project.Name, createdHook.URL)
		}

	}
}
