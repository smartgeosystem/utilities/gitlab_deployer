package cmd

import (
	"github.com/sherifabdlnaby/configuro"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/smartgeosystem/utilities/gitlab_deployer/conf"
	"os"
)

var cfgFile string
var cfg = conf.GetDefaultAppConfig()

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "gitlab-deployer",
	Short:   "GitLab deployer",
	Long:    `Simple service for deploy artifacts on ci/cd events`,
	Version: "2.0.0",
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Errorln(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initCommon)

	// Global flags
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "config.yml", "local config file")
}

func initCommon() {}

func loadConfigs() {
	log.Println("Load config...")
	loader, err := configuro.NewConfig(
		configuro.WithLoadFromConfigFile(cfgFile, true),
		configuro.WithoutLoadFromEnvVars(),
		configuro.WithExpandEnvVars(),
	)
	if err != nil {
		log.Errorln("Configuration error:", err)
		os.Exit(-1)
	}
	err = loader.Load(&cfg)
	if err != nil {
		log.Errorln("Load configuration error:", err)
		os.Exit(-2)
	}
}
