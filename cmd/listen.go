package cmd

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/smartgeosystem/utilities/gitlab_deployer/event_handler"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// listenCmd represents the serve command
var listenCmd = &cobra.Command{
	Use:   "listen",
	Short: "Start listening gitlab events",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		startListen()
	},
}

func init() {
	rootCmd.AddCommand(listenCmd)
}

func startListen() {
	// Global and local configuration
	loadConfigs()
	srv := &http.Server{Addr: fmt.Sprintf(":%d", cfg.Service.Port)}
	http.HandleFunc("/", event_handler.HandleGitLabEvent(cfg.Service.Debug, cfg.GitLab.Token, cfg.Actions, cfg.Notification))

	// Start http server
	go func() {
		log.Printf("Start http server on address :%d", cfg.Service.Port)
		err := srv.ListenAndServe()
		if err != nil {
			log.Fatalln("HTTP Server error: ", err)
		}
	}()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	select {
	case res := <-sigs:
		log.Println("OS Signal: ", res)
		// Close all and exit
		log.Println("Exiting")
		err := srv.Close()
		if err != nil {
			log.Fatalln("HTTP Server error on exit: ", err)
		}
		os.Exit(0)
	}
}
