package conf

import "gitlab.com/smartgeosystem/utilities/gitlab_deployer/notification"

type AppConfig struct {
	Service      ServiceConfig
	GitLab       GitLabInstanceConfig
	Notification NotificationConfig
	Actions      []ProjectActions
}

func GetDefaultAppConfig() AppConfig {
	eventInfo := "<b>Repo:</b> {{.Project.Name}}:{{.ObjectAttributes.Ref}}\n<b>User:</b> {{.User.UserName}}\n<b>Message:</b> {{.Commit.Message}}"
	defaultMessages := notification.PipelineMessages{
		RunningMessage:       "<b><u>New commit</u></b> 🆕\n" + eventInfo + "\n\n<b>Build...</b>",
		FailedMessage:        "<b><u>Build error</u></b> 😢\n" + eventInfo + "\n\n<b>!!! Check log at {{.Project.WebURL}}/-/jobs !!!</b>",
		SuccessMessage:       "<b><u>Build complete</u></b> 😊\n" + eventInfo + "\n\n<b>Deploy...</b>",
		DeploySuccessMessage: "<b><u>Deployment successful completed</u></b> 👍\n" + eventInfo,
		DeployFailedMessage:  "<b><u>Deployment failed</u></b> 😱\n" + eventInfo + "\n\n<b>!!! Check server log !!!</b>",
	}

	return AppConfig{
		Service: ServiceConfig{
			Port:  7979,
			Debug: false,
		},
		GitLab: GitLabInstanceConfig{Address: "https://gitlab.com"},
		Notification: NotificationConfig{
			Telegram: notification.TelegramBot{
				ParseMode: notification.HtmlParseMode,
			},
			Messages: defaultMessages,
			Retry:    3,
		},
		Actions: []ProjectActions{},
	}
}
