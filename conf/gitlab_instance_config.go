package conf

type GitLabInstanceConfig struct {
	Address string
	Token   string
}
