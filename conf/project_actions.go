package conf

type ProjectActions struct {
	ProjectName string `config:"project"`
	Actions     map[string]string
}
