package conf

import "gitlab.com/smartgeosystem/utilities/gitlab_deployer/notification"

type NotificationConfig struct {
	Telegram notification.TelegramBot `config:"telegram"`
	Messages notification.PipelineMessages
	Retry    int
}
