package conf

type ServiceConfig struct {
	Port  int
	Debug bool
}
