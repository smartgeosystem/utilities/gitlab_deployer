package main

import (
	"gitlab.com/smartgeosystem/utilities/gitlab_deployer/cmd"
)

func main() {
	cmd.Execute()
}
