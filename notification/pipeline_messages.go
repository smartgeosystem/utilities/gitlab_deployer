package notification

import (
	"bytes"
	log "github.com/sirupsen/logrus"
	"html/template"
	"strings"
)

type PipelineMessages struct {
	RunningMessage       string `config:"running"`
	FailedMessage        string `config:"failed"`
	SuccessMessage       string `config:"success"`
	DeploySuccessMessage string `config:"deploy_success"`
	DeployFailedMessage  string `config:"deploy_failed"`
}

func (p PipelineMessages) FillMessage(message string, ctx interface{}) (string, error) {
	message = strings.Replace(message, `\n`, "\n", -1)

	var buff bytes.Buffer
	templ, err := template.New("Message").Parse(message)
	if err != nil {
		log.Errorln(err)
		return "", err
	}
	err = templ.Execute(&buff, ctx)
	if err != nil {
		log.Errorln(err)
		return "", err
	}
	return buff.String(), nil
}
