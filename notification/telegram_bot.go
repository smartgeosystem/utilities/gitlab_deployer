package notification

import (
	"bytes"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

var (
	NotConfiguredError = fmt.Errorf("telegram Bot not configured")
)

type ParseMode string

const (
	HtmlParseMode       ParseMode = "HTML"
	MarkdownV2ParseMode ParseMode = "MarkdownV2"
)

type TelegramBot struct {
	BotId     string    `config:"bot_id"`
	ChatIds   []string  `config:"chat_ids"`
	ParseMode ParseMode `config:"parse_mode"`
}

const ApiUrl = "https://api.telegram.org/"

func (t TelegramBot) getBotUrl() string {
	return ApiUrl + "bot" + t.BotId
}

func (t TelegramBot) getCommandUrl(command string) string {
	return t.getBotUrl() + "/" + command
}

func (t TelegramBot) SendMessage(message string) error {
	if t.BotId == "" || t.ChatIds == nil {
		log.Errorln(NotConfiguredError)
		return NotConfiguredError
	}
	url := t.getCommandUrl("sendMessage")

	for _, chatId := range t.ChatIds {
		values := map[string]string{
			"chat_id":    chatId,
			"text":       message,
			"parse_mode": string(t.ParseMode),
		}
		jsonValue, _ := json.Marshal(values)
		resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
		if err != nil {
			log.Errorln("Error on send telegram message:", err)
			return err
		}
		if resp.StatusCode >= 400 {
			body, _ := ioutil.ReadAll(resp.Body)
			log.Errorln("Error on send telegram message:", resp.Status, ":", string(body))
			log.Errorln("Send message:", string(jsonValue))
			return err
		}
	}
	return nil
}

func (t TelegramBot) SendMessageWithRetry(message string, retry int) error {
	var err error = nil
	for i := 0; i < retry; i++ {
		err = t.SendMessage(message)
		if err == nil {
			break
		}
	}
	return err
}
