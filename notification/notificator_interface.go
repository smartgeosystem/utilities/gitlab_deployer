package notification

type Notificator interface {
	SendMessage(message string) error
	SendMessageWithRetry(message string, retry int) error
}
